#How to implement Google Maps SlidingUpPanel

This project is based in google maps application from google.

##Activity View

This class is an interface to communicate our pane listener with activity or fragment where we are using the animation.

##InfoManager

Class who handles nested scroll and panel dragging, avoiding that scrollview scrolls if the pane is not expanded

##Slide listener

It manages image translation, header animation and toolbar shown

