package mapanimation.eeema.com.googlemapslideableanimation.adapter;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mapanimation.eeema.com.googlemapslideableanimation.R;


/**
 * Created by emanuel on 10/02/16.
 */
public class RecyclerAdapter extends RecyclerView.Adapter {

    private static final int ITEM_COUNT = 1;

    @Override    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView layout = (RecyclerView) LayoutInflater.from(parent.getContext()).inflate(R.layout.slide_body_layout, parent);
        NestedScrollView container = (NestedScrollView) layout.findViewById(R.id.container_info);

        return new RecyclerHolder(container);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return ITEM_COUNT;
    }

    public static class RecyclerHolder extends RecyclerView.ViewHolder{

        public RecyclerHolder(View itemView) {
            super(itemView);
        }
    }
}
