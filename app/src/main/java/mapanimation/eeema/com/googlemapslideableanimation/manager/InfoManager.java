package mapanimation.eeema.com.googlemapslideableanimation.manager;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by emanuel on 10/02/16.
 */
public class InfoManager extends LinearLayoutManager {

    private SlidingUpPanelLayout pane;


    public InfoManager(Context context, SlidingUpPanelLayout pane) {
        super(context);
        this.pane = pane;
    }

    @Override
    public boolean canScrollVertically() {
      //It should be  pane.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED, but I don't know why this fucking shit doesn't work
      return pane.getPanelState() != SlidingUpPanelLayout.PanelState.ANCHORED;
    }

    @Override
    public boolean canScrollHorizontally() {
        return false;
    }
}
