package mapanimation.eeema.com.googlemapslideableanimation.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.Window;

/**
 * Created by emanuel on 10/02/16.
 */
public class Utils {


    public static Rect getVisibleScreen(Activity activity){

        Rect visibleScreen= new Rect();
        Window window= activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(visibleScreen);

        return visibleScreen;

    }

    public static Rect getVisibleScreenWithoutNotificationBar(Activity activity){

        Rect visibleScreen = getVisibleScreen(activity);
        int statusBarHeight = getStatusBarHeight(activity);
        visibleScreen.top += statusBarHeight;

        return visibleScreen;

    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int getToolbarHeight(Context context){
        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)){
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,context.getResources().getDisplayMetrics());
        }

        return actionBarHeight;
    }

}
