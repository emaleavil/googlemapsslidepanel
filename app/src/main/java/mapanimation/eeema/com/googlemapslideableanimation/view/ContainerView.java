package mapanimation.eeema.com.googlemapslideableanimation.view;

/**
 * Created by emanuel on 4/02/16.
 *
 * Interface encargado de la comunicación entre el listener del panel deslizante y la activity o fragment que lo contenga.
 */
public interface ContainerView {

    void showFloatingButton();
    void hideFloatingButton();
    void setFloatingButtonPosY(int top);

    void translateImageView(float y);
    void setImageToTop(int top);
    float getImageCurrentYPos();
    void showImage();
    void hideImage();

    void showToolbar();
    void hideToolbar();
    void setToolbarTitle(boolean covering);
    void changeToolbarColor(int resColor);

    void selectedHeader();
    void unselectedHeader();
    void changeButtonState(boolean selected);


}
