package mapanimation.eeema.com.googlemapslideableanimation;

import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import mapanimation.eeema.com.googlemapslideableanimation.adapter.RecyclerAdapter;
import mapanimation.eeema.com.googlemapslideableanimation.listener.SlidingListener;
import mapanimation.eeema.com.googlemapslideableanimation.manager.InfoManager;
import mapanimation.eeema.com.googlemapslideableanimation.utils.Utils;
import mapanimation.eeema.com.googlemapslideableanimation.view.ContainerView;

public class MainActivity extends AppCompatActivity implements ContainerView, OnClickListener{

    private RelativeLayout parent;
    private SlidingUpPanelLayout pane;
    private FloatingActionButton fabButton;
    private ImageView backImage;
    private Toolbar toolbar;
    private Button showPanel;
    private ViewSwitcher header;
    private RelativeLayout mainView;
    private RecyclerView infoContainer;
    private LinearLayout secondaryView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parent = (RelativeLayout) findViewById(R.id.parent);
        pane = (SlidingUpPanelLayout) findViewById(R.id.sliding_pane);
        pane.setOverlayed(true);
        mainView = (RelativeLayout) findViewById(R.id.main);

        //Setting recycler view
        infoContainer = (RecyclerView) findViewById(R.id.recycler_info_container);
        infoContainer.setAdapter(new RecyclerAdapter());
        infoContainer.setLayoutManager(new InfoManager(this, pane));

        secondaryView = (LinearLayout) findViewById(R.id.secondary);

        fabButton = (FloatingActionButton) findViewById(R.id.floating_button);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        backImage = (ImageView) mainView.findViewById(R.id.background_slideable_image);
        header = (ViewSwitcher) secondaryView.findViewById(R.id.header_switcher);

        showPanel = (Button) mainView.findViewById(R.id.map_object);
        showPanel.setOnClickListener(this);

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus){
            pane.setPanelSlideListener(new SlidingListener(this, pane, Utils.getToolbarHeight(this)));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void translateImageView(float y) {
        ViewCompat.setTranslationY(backImage, y);
    }

    @Override
    public void setImageToTop(int top) {
        if(backImage.getY()!= top){
            ViewCompat.setY(backImage, top);

        }
    }

    @Override
    public float getImageCurrentYPos() {
        return ViewCompat.getY(backImage);
    }

    @Override
    public void showImage() {
        backImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideImage() {
        backImage.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showFloatingButton() {
        fabButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideFloatingButton() {
        fabButton.setVisibility(View.GONE);
    }

    @Override
    public void setFloatingButtonPosY(int top) {
        ViewCompat.setTranslationY(fabButton, top - (fabButton.getHeight() / 2));
    }

    @Override
    public void showToolbar() {
        if(getSupportActionBar() != null){
            getSupportActionBar().show();
        }
    }

    @Override
    public void hideToolbar() {
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
    }

    @Override
    public void setToolbarTitle(boolean covering) {
        View currentView = header.getCurrentView();
        TextView tv = (TextView) currentView.findViewById(R.id.header_title);
        toolbar.setTitle(tv.getText().toString());
        setSupportActionBar(toolbar);
    }

    @Override
    public void changeToolbarColor(int resColor) {
        if(!isActionbarSupported()) return;

        modifyActionBarColor(getColorFromRes(resColor));
    }
    private void modifyActionBarColor(int color){
        ActionBar actionBar = ((AppCompatActivity) this).getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(color));
    }

    public void unselectedHeader(){
        int currentViewIndex = header.getDisplayedChild();
        if(currentViewIndex != 0){
            header.showPrevious();
        }
    }

    public void selectedHeader(){
        int currentViewIndex = header.getDisplayedChild();
        if(currentViewIndex != 1){
            header.showNext();
        }
    }



    public void changeButtonState(boolean selected){
        int resColorId = selected? android.R.color.white : android.R.color.holo_blue_bright;
        fabButton.setBackgroundTintList(ColorStateList.valueOf(getColorFromRes(resColorId)));

        int resIconId  = !selected ? R.drawable.ic_add_white_24dp :  R.drawable.ic_add_black_24dp ;

        fabButton.setImageDrawable(getDrawableFromRes(resIconId));
    }

    private Drawable getDrawableFromRes(int resDrawable){
        Drawable drawable = getResources().getDrawable(resDrawable);
        return drawable;
    }


    @Override
    public void onClick(View v) {
        if(pane.getPanelState()== SlidingUpPanelLayout.PanelState.HIDDEN){
            pane.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            showPanel.setText(getText(R.string.hide_panel));

        }else{
            pane.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            showPanel.setText(getText(R.string.show_panel));
        }
    }



    private boolean isActionbarSupported(){
        return this instanceof  AppCompatActivity && ((AppCompatActivity) this).getSupportActionBar() != null;
    }

    private int getColorFromRes(int resColor){
        int color  = getResources().getColor(resColor);
        return color;
    }

    public SlidingUpPanelLayout.PanelState getCurrentPanelState(){
        return pane.getPanelState();
    }

}
