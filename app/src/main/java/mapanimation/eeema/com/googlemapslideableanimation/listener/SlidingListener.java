package mapanimation.eeema.com.googlemapslideableanimation.listener;

import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import mapanimation.eeema.com.googlemapslideableanimation.R;
import mapanimation.eeema.com.googlemapslideableanimation.view.ContainerView;

/**
 * Created by emanuel on 4/02/16.
 */
public class SlidingListener implements SlidingUpPanelLayout.PanelSlideListener {

    private ContainerView view;


    public static final int SLIDE_UP = 1;
    public static final int SLIDE_DOWN = -1;
    public static final int NO_SLIDE = 0;

    private static final float OFFSET_HIDE_FLOATING_BUTTON = 0.7f;


    private int slidingDirection;
    private float previousOffset;

    private SlidingUpPanelLayout slidePanel;
    private RelativeLayout parent;
    private ImageView backImage;


    private int toolbarHeight;
    private int parentTopMargin;
    private int imageHeight;


    public SlidingListener(ContainerView view, SlidingUpPanelLayout slidePanel, int toolbarHeight) {
        this.parent = (RelativeLayout) slidePanel.getParent();
        this.view = view;
        this.slidePanel = slidePanel;
        this.toolbarHeight = toolbarHeight;
        loadParentTopMargin();
        loadBackImageHeight();
        calculateAnchorPoint();

    }

    private void loadParentTopMargin(){
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) parent.getLayoutParams();
        this.parentTopMargin = Math.abs(params.topMargin);
    }

    private void loadBackImageHeight(){
        RelativeLayout mainView = (RelativeLayout) slidePanel.findViewById(R.id.main);
        backImage = (ImageView) mainView.findViewById(R.id.background_slideable_image);
        imageHeight = backImage.getHeight();
    }

    private void calculateAnchorPoint(){
        float slideableDistance = slidePanel.getHeight() - slidePanel.getPanelHeight();
        float anchorPoint = 1.0f - (slideableDistance - imageHeight - parentTopMargin) /slideableDistance;
        slidePanel.setAnchorPoint(anchorPoint);
    }

    @Override
    public void onPanelSlide(View panel, float slideOffset) {
        manageHeader(slideOffset);
        manageToolbar(panel, slideOffset);
        manageImage(panel, slideOffset);
        manageFloatingButton(panel, slideOffset);

        setPreviousOffset(slideOffset);
    }


    @Override
    public void onPanelCollapsed(View panel) {
        view.showImage();
        view.hideToolbar();
        view.setImageToTop(panel.getTop());


    }

    @Override
    public void onPanelExpanded(View panel) {
        translateImageToTop();
    }

    @Override
    public void onPanelAnchored(View panel) {
        translateImageToTop();
        view.changeToolbarColor(android.R.color.transparent);
        view.showToolbar();
    }

    @Override
    public void onPanelHidden(View panel) {
        view.hideFloatingButton();
        view.hideImage();
        view.changeToolbarColor(android.R.color.holo_purple);
        view.showToolbar();
    }


    private float getAnchorPoint() {
        return slidePanel.getAnchorPoint();
    }

    private void setPreviousOffset(float offset) {
        previousOffset = offset;
    }

    private int getSlidingDirection(float newOffset) {
        slidingDirection = NO_SLIDE;

        if (previousOffset < newOffset) {
            slidingDirection = SLIDE_UP;

        } else if (previousOffset > newOffset) {
            slidingDirection = SLIDE_DOWN;

        }

        return slidingDirection;
    }

    private void manageToolbar(View panel, float offset) {
        if (offset < getAnchorPoint()) {
            view.hideToolbar();

        } else if (panel.getTop() >  (parentTopMargin + toolbarHeight/2)) {
            view.showToolbar();
            view.changeToolbarColor(android.R.color.transparent);
            view.setToolbarTitle(false);

        } else {
            view.setToolbarTitle(true);
            view.changeToolbarColor(android.R.color.holo_purple);
        }
    }

    private void manageImage(View panel, float offset) {

        float posYImage = view.getImageCurrentYPos();
        float dy = Math.abs(panel.getTop());
        int slidePanelTop =  slidePanel.getTop() + parentTopMargin;

        if (getSlidingDirection(offset) == SLIDE_UP) {
            if (posYImage <= slidePanelTop) {
                view.setImageToTop(slidePanelTop);
            } else {

                float translation = ViewCompat.getY(panel)  - (offset * dy / getAnchorPoint());
                view.translateImageView(translation);
            }

        } else if(getSlidingDirection(offset) == SLIDE_DOWN) {

            if (offset >= getAnchorPoint()) {
                view.setImageToTop(slidePanelTop);
            } else {
                float translation = ViewCompat.getY(panel) + parentTopMargin - (offset * dy / getAnchorPoint());
                view.translateImageView(translation);

            }

        }
    }

    private void manageFloatingButton(View panel, float offset) {

        if (previousOffset >= OFFSET_HIDE_FLOATING_BUTTON &&
                offset <= OFFSET_HIDE_FLOATING_BUTTON &&
                getSlidingDirection(offset) == SLIDE_DOWN) {

            view.showFloatingButton();
            view.setFloatingButtonPosY(panel.getTop());

        } else if (offset < OFFSET_HIDE_FLOATING_BUTTON) {

            view.showFloatingButton();
            view.setFloatingButtonPosY(panel.getTop());

        } else {
            view.hideFloatingButton();
        }
    }

    void manageHeader(float offset){
        if(offset > 0.0f){
            view.selectedHeader();
        }else{
            view.unselectedHeader();
        }
        view.changeButtonState(offset > 0.0f);
    }

    private void translateImageToTop() {
        if (slidePanel.getPanelState() != SlidingUpPanelLayout.PanelState.DRAGGING) {
            view.setImageToTop(slidePanel.getTop() + parentTopMargin);

        }
    }
}
