package mapanimation.eeema.com.googlemapslideableanimation.utils;

/**
 * Created by Emanuel on 19/02/2016.
 */
public class PanelStateKeys {

    public static final String PANEL_STATE_KEY = "panel_state";

    public static final int PANEL_HIDDEN = 0;
    public static final int PANEL_COLLAPSED = 1;
    public static final int PANEL_ANCHORED = 2;
    public static final int PANEL_EXPANDED = 3;
}
