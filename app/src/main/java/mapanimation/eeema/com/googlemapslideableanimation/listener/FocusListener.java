package mapanimation.eeema.com.googlemapslideableanimation.listener;

/**
 * Created by emanuel on 12/02/16.
 *
 * Only used to communicate activity with fragment if panel is stored in fragment
 */
public interface FocusListener {
    void windowsFocusChanged(boolean hasFocus);
}
