package mapanimation.eeema.com.googlemapslideableanimation.utils;

import android.os.Bundle;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by Emanuel on 19/02/2016.
 */
public class PanelStateBackup {

    public static void saveCurrentPanelState(SlidingUpPanelLayout pane, Bundle outState){
        SlidingUpPanelLayout.PanelState currentState = pane.getPanelState();
        int state = convertState(currentState);
        outState.putInt(PanelStateKeys.PANEL_STATE_KEY, state);

    }

    private static int convertState(SlidingUpPanelLayout.PanelState state){
        switch (state){
            case EXPANDED:
                return PanelStateKeys.PANEL_EXPANDED;
            case COLLAPSED:
                return PanelStateKeys.PANEL_COLLAPSED;
            case ANCHORED:
                return PanelStateKeys.PANEL_ANCHORED;
            case HIDDEN:
                return PanelStateKeys.PANEL_HIDDEN;
        }

        return -1;
    }
}
